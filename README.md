# ST3V3NHUNT GitHub Pages Blog

**[st3v3nhunt.github.io](https://st3v3nhunt.github.io)** is a personal blog created by [@st3v3nhunt](https://twitter.com/st3v3nhunt) based on the [Minimal Mistakes](https://github.com/mmistakes/minimal-mistakes) theme for the [Jekyll](http://jekyllrb.com/) static site generator, hosted on [GitHub Pages](https://pages.github.com/) infrastructure.


The blog is mostly concerned with software development and cycling.

Run the site via `bundle exec jekyll serve`
Build the site via `bundle exec jekyll build`
